package pl.pmajewski.dropgame.utils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class GlobalProperties {

    public static final boolean DEBUG_MODE = true;

    public static final float APP_WIDTH = 8;
    public static final float APP_HEIGHT = 16;

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, 0);
    public static final float WORLD_TO_SCREEN = 1;
    public static final float TIME_STEP = 1/60f;

    public static final float BALL_X = APP_WIDTH/2;
    public static final float BALL_Y = 14;
    public static final float BALL_WIDTH = 0.75f; // in Box2d world
    public static final float BALL_HEIGHT = 0.75f; // in Box2d world
    public static final float BALL_VELOCITY_X = 0f;
    public static final float BALL_VELOCITY_Y = -1f;
    public static final float BALL_GRAVITY_SCALE = 1f;
    public static final float BALL_DENSITY = 1f;
    public static final float BALL_MAX_HORIZONTAL_JUMP = 0.5f;
    public static final String BALL_ASSETS_ID = "jump.png";

    public static final float PLATFORM_DENSITY = 1f;
    public static final String PLATFORM_TEXTURE = "brick.png";
    public static final float PLATFORM_LINE_DISTANCE = 0.75F; // box2d metric value

    {
        if(DEBUG_MODE) {
            Gdx.app.setLogLevel(Application.LOG_DEBUG);
        }

    }
}
