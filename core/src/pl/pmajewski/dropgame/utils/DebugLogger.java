package pl.pmajewski.dropgame.utils;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

import java.util.HashMap;

public class DebugLogger {

    private static HashMap<String, Long> timestamps = new HashMap<>();

    public static void debug(String message, String... tags) {
        debug(message, prettyTags(tags));
    }
    public static void debug(String message, String tag) {
        if (GlobalProperties.DEBUG_MODE) {
            if(Gdx.app.getType() == Application.ApplicationType.Android) {
//                Gdx.app.debug(tag, message);
                System.out.println(tag+" "+message);
            } else {
                System.out.println(tag+" "+message);
            }
        }
    }

    /**
     *
     * @param howOften - min interval between same tag printing in millisecond
     * @param message - message
     * @param tag - tags
     */
    public static void debug(long howOften, String message, String... tag) {
        if (GlobalProperties.DEBUG_MODE) {
            String tags = prettyTags(tag);

            if(timestamps.get(tags) != null
                    && System.currentTimeMillis() - timestamps.get(tags) > howOften) {
                timestamps.put(tags, System.currentTimeMillis());
                debug(message, tags);
            } else if(timestamps.get(tags) == null) {
                timestamps.put(tags, System.currentTimeMillis());
            }

        }
    }

    /**
     * @param howOften - min interval between same tag printing in sec
     */
    public static void debug(int howOften, String message, String... tags) {
        long interval = howOften * 1000l;
        debug(interval, message, tags);
    }

    private static String prettyTags(String[] tags) {
        StringBuilder sb = new StringBuilder();
        for (String tag : tags) {
            sb.append("[").append(tag).append("]").append(" ");
        }
        return sb.toString();
    }
}
