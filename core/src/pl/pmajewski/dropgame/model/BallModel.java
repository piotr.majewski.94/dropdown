package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import java.util.LinkedList;
import java.util.List;

import pl.pmajewski.dropgame.utils.DebugLogger;
import pl.pmajewski.dropgame.utils.GameManager;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public class BallModel extends GameActor implements PositionSource {

    private TextureRegion characterTexture;
    private List<PositionListener> positionListeners = new LinkedList<>();

    public BallModel(World world) {
        super(world);
        create(world);
        setUpTexture();
    }

    @Override
    protected Float getBodyWidth() {
        return GlobalProperties.BALL_WIDTH;
    }

    @Override
    protected Float getBodyHeight() {
        return GlobalProperties.BALL_HEIGHT;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        notifyListeners(body.getPosition());
        DebugLogger.debug(500l,"bodyPosition="+body.getPosition()+" linearVelocity="+body.getLinearVelocity()+" angle="+body.getAngle(), "BALL");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        drawHelper(batch, characterTexture);
    }

    private void setUpTexture() {
        Texture texture = GameManager.getInstance().getAssetManager()
                .get(GlobalProperties.BALL_ASSETS_ID, Texture.class);
        this.characterTexture = new TextureRegion(texture);
    }

    private void create(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(GlobalProperties.BALL_X, GlobalProperties.BALL_Y ));


        CircleShape shape = new CircleShape();
        shape.setRadius(GlobalProperties.BALL_WIDTH/2/2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.BALL_DENSITY;
        fixtureDef.restitution = 0.2f;
        fixtureDef.friction = 0f;

        Body body = world.createBody(bodyDef);
        body.setLinearVelocity(new Vector2(GlobalProperties.BALL_VELOCITY_X, GlobalProperties.BALL_VELOCITY_Y));
        body.setGravityScale(GlobalProperties.BALL_GRAVITY_SCALE);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(this);
        body.setUserData(this);
        body.resetMassData();

        shape.dispose();
        this.body = body;
    }

    public void addPositionListener(PositionListener listener) {
        positionListeners.add(listener);
    }

    private void notifyListeners(Vector2 position) {
        for (PositionListener listener: positionListeners) {
            listener.notify(this, position);
        }
    }

    public void forceRight() {
        body.applyForceToCenter(new Vector2(2f,0f), false);
    }

    public void forceLeft() {
        body.applyForceToCenter(new Vector2(-2f,0f), false);
    }

    public void moveX(float positionX) {
        body.setTransform(new Vector2(positionX, body.getPosition().y), 0f);
    }
}
