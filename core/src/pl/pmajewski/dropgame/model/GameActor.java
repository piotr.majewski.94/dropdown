package pl.pmajewski.dropgame.model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

import lombok.Getter;
import pl.pmajewski.dropgame.utils.GlobalProperties;

public abstract class GameActor extends Actor {

    @Getter
    protected Body body;
    private World world;

    protected abstract Float getBodyWidth();
    protected abstract Float getBodyHeight();

    public GameActor(World world) {
        this.world = world;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(body.getUserData() != null) {
            updateRectangle();
        }
    }

    public Vector2 getBodyPosition() {
        return this.body.getPosition();
    }

    private void updateRectangle() {
        setX(transformToScreen(body.getPosition().x - getBodyWidth() / 2));
        setY(transformToScreen(body.getPosition().y - getBodyHeight() / 2));
        setWidth(transformToScreen(getBodyWidth()));
        setHeight(transformToScreen(getBodyHeight()));
        setRotation((float) Math.toDegrees(body.getAngle()));
    }

    protected float transformToScreen(float n) {
        return GlobalProperties.WORLD_TO_SCREEN * n;
    }

    protected void drawHelper(Batch batch, TextureRegion textureRegion) {
        batch.draw(textureRegion, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1, 1, getRotation());
    }

    @Override
    public boolean remove() {
        world.destroyBody(body);
        return super.remove();
    }
}
