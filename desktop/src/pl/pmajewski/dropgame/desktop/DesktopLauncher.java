package pl.pmajewski.dropgame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import pl.pmajewski.dropgame.GameMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 300;
		config.height = 600;
		config.samples = 4;
		config.allowSoftwareMode=true;
		config.foregroundFPS = 60;
		new LwjglApplication(new GameMain(), config);
	}
}
